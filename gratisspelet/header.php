<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php $header_image = get_header_image(); ?>
    <header class="gratisspelet-header" id="gratisspelet-header"  style="background: #000000 url(<?php echo $header_image ?>) center bottom 120px no-repeat; background-size: 100% auto">
        <nav class="navbar">
            <div class="container">
                <div class="rows">
                    <div class="navbar-header">
                        <a class="navbar-brand logo" href="<?php echo home_url();?>">
                            <img src="<?php echo get_theme_logo(); ?>" alt="<?php bloginfo('name'); ?>" />
                        </a>
                        <button class="navbar-toggle" id="menu-button" data-toggle="collapse" data-target="#gratisspelet-menu-nav">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span><span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
            </div>
        </nav>
        <div class="collapse navbar-collapse" id="gratisspelet-menu-nav">
           <div class="container">
                <?php
                wp_nav_menu( array(
                    'menu' => 'top_menu',
                    'theme_location' => 'primary',
                    'depth' => 2,
                    'container' => false,
                    'menu_class' => 'nav navbar-nav',
                    'walker' => new wp_bootstrap_navwalker() ) );
                ?>
                <button class="search-toggle" id="search-button" data-toggle="collapse" data-target="#gratisspelet-search"></button>
                <div id="gratisspelet-search" class="gratisspelet-search-form">
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>
    </header>
    <div class="gratisspelet-banner">
        <div style="background: #000000 url(<?php echo $header_image ?>) center bottom 20px no-repeat; background-size: 100% auto" class="banner-image-mobile"></div>
    </div>

    <div class="container gratisspelet-base-container">