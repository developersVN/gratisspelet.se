	</div>
	<footer class="gratisspelet-footer" id="gratisspelet-footer">
        <div class="container">
            <div class="gratisspelet-copyright"><?php echo date('Y') ?>&copy; Gratisspelet.se</div>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>